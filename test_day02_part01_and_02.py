import pytest
from AOC_day2_part01_and_02 import run_intcode_program, find_noun_and_verb

@pytest.mark.parametrize("input_program, expected_program", [
    ([1, 0, 0, 0, 99], [2, 0, 0, 0, 99]),
    ([2, 3, 0, 3, 99], [2, 3, 0, 6, 99]),
    ([2, 4, 4, 5, 99, 0], [2, 4, 4, 5, 99, 9801]),
    ([1, 1, 1, 4, 99, 5, 6, 0, 99], [30, 1, 1, 4, 2, 5, 6, 0, 99]),
])
def test_run_intcode_program(input_program, expected_program):
    assert run_intcode_program(input_program) == expected_program

test_program_data = [1, 0, 0, 0, 99]


@pytest.mark.parametrize("program, target_output, expected_noun_verb", [
    (test_program_data, 2, (0, 0)),
])
def test_find_noun_and_verb(program, target_output, expected_noun_verb):
    assert find_noun_and_verb(program, target_output) == expected_noun_verb
