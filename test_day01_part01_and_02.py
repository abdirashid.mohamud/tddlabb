import unittest
from AOC_day1_part01_and_02 import calculate_fuel, calculate_fuel_recursive 

class TestAOC2019Day1(unittest.TestCase):

    def test_calculate_fuel(self):
        self.assertEqual(calculate_fuel(12), 2)
        self.assertEqual(calculate_fuel(14), 2)
        self.assertEqual(calculate_fuel(1969), 654)
        self.assertEqual(calculate_fuel(100756), 33583)

    def test_calculate_fuel_recursive(self):
        self.assertEqual(calculate_fuel_recursive(14), 2)
        self.assertEqual(calculate_fuel_recursive(1969), 966)
        self.assertEqual(calculate_fuel_recursive(100756), 50346)

if __name__ == '__main__':
    unittest.main()
