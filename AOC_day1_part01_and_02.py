def calculate_fuel(mass):
    return mass // 3 - 2

def calculate_fuel_recursive(mass):
    fuel = calculate_fuel(mass)
    if fuel <= 0:
        return 0
    return fuel + calculate_fuel_recursive(fuel)

def main():
    # Input file path
    input_file = 'input.txt'
    
    with open(input_file, 'r') as file:
        masses = [int(line.strip()) for line in file.readlines()]
    
    # Part 1
    total_fuel = sum(calculate_fuel(mass) for mass in masses)
    print(f"Part 1: Total fuel required: {total_fuel}")
    
    # Part 2
    total_fuel_recursive = sum(calculate_fuel_recursive(mass) for mass in masses)
    print(f"Part 2: Total fuel required considering fuel mass: {total_fuel_recursive}")

if __name__ == "__main__":
    main()
