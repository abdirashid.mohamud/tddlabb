def run_intcode_program(program):
    pc = 0
    while program[pc] != 99:
        opcode = program[pc]
        a, b, dest = program[pc + 1], program[pc + 2], program[pc + 3]

        if opcode == 1:
            program[dest] = program[a] + program[b]
        elif opcode == 2:
            program[dest] = program[a] * program[b]
        else:
            raise ValueError(f"Invalid opcode: {opcode}")

        pc += 4
    return program

def find_noun_and_verb(program, target_output):
    for noun in range(100):
        for verb in range(100):
            test_program = program.copy()
            test_program[1], test_program[2] = noun, verb

            if run_intcode_program(test_program)[0] == target_output:
                return noun, verb

def main():
    input_file = 'input_02.txt'
   
    with open(input_file, 'r') as file:
        original_program = [int(x) for x in file.read().strip().split(',')]

    # Part 1
    program = original_program.copy()
    program[1], program[2] = 12, 2
    result = run_intcode_program(program)
    print(f"Part 1: Value at position 0: {result[0]}")

    # Part 2
    target_output = 19690720
    noun, verb = find_noun_and_verb(original_program, target_output)
    print(f"Part 2: 100 * noun + verb: {100 * noun + verb}")

if __name__ == "__main__":
    main()
